import java.io.PrintWriter
import java.io.File
import java.util.Scanner
import scala.collection.mutable.ArrayBuffer
import scala.io.{BufferedSource, Source}
import scala.util.Sorting
import scala.util.control.Breaks._

/**
 * Created by Alexandru Grigoroi on 05/02/14. 
 */

object Main {

  def readData(file: BufferedSource): Array[Cell] = {
    val cells:ArrayBuffer[Cell] = new ArrayBuffer()
    breakable {
      for(line <- file.getLines()) {
        val coordinates = line.split(",").map(x => x.toInt)
        if( (coordinates(0) == -1) && (coordinates(1) == -1) )
          break()
        cells.append(new Cell(coordinates(0), coordinates(1)))
      }
    }
    cells.toArray
  }

  def writeToFile(file: String, aliveCells: Array[Cell]) {
    val writer = new PrintWriter(file)
    for(cell <- aliveCells)
      writer.println(cell.x.toString + "," + cell.y.toString)
    writer.println("-1,-1")
    writer.close()
  }

  def writeToStd(aliveCells: Array[Cell]) {
    for(cell <- aliveCells) {
      println(cell.x.toString + "," + cell.y.toString)
    }
    println("-1,-1")
  }


  def main(args: Array[String]) {
    var aliveCells: Array[Cell] = null
    if(args.length == 0)
      aliveCells = readData(Source.stdin)
    else
      aliveCells = readData(Source.fromFile(args(0)))
    val state: State = new State(aliveCells)
    state.next()
    val cells = state.getAliveCells
    Sorting.quickSort(cells)
    if(args.length < 2)
      writeToStd(cells)
    else
      writeToFile(args(1), cells)
  }

}
