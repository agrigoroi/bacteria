/**
 * Created by Alexandru Grigoroi on 04/02/14.
 */

import scala.collection.mutable.ArrayBuffer

class Cell (cx: Long, cy: Long) extends Ordered[Cell] {

  // The x and y coordinates of the cell
  val x: Long = cx
  val y: Long = cy

  // Return an array of all (eight) neighbors of this Cell
  def getNeighbors: Array[Cell] = {
    val neighbors: ArrayBuffer[Cell] = ArrayBuffer()
    for(i <- 0 to 7) {
      // Check if the coordinates of the neighbor are positive
      if( (x + Cell.DELTA_X(i) >= 0 ) && (y + Cell.DELTA_Y(i) >= 0) ) {
        neighbors.append(new Cell(x + Cell.DELTA_X(i), y + Cell.DELTA_Y(i)))
      }
    }
    neighbors.toArray
  }

  // Cell a is bigger than Cell b iff a.x > b.x or (a.x==b.x and a.y > b.y)
  override def compare(that: Cell): Int = {
    if (this.x == that.x)
      this.y.compare(that.y)
    else
      this.x.compare(that.x)
  }

  def canEqual(other: Any): Boolean = other.isInstanceOf[Cell]

  // Two cells are equal when both coordinates are the same
  override def equals(other: Any): Boolean = other match {
    case that: Cell =>
      (that canEqual this) &&
        x == that.x &&
        y == that.y
    case _ => false
  }

  override def hashCode(): Int = (x * Cell.LARGE_PRIME + y).toInt
}


object Cell {

  // Changes in coordinates to give all the neighbors
  val DELTA_X: Array[Int] = Array( 0, 0,  1, 1, 1, -1, -1, -1)
  val DELTA_Y: Array[Int] = Array(-1, 1, -1, 0, 1, -1,  0,  1)

  // A large prime number (used in hashCode)
  val LARGE_PRIME: Int =  1046527


}

