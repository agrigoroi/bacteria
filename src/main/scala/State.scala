import scala.collection.mutable.HashSet
/**
 * Created by Alexandru Grigoroi on 04/02/14. 
 */

class State() {
  // Set containing all the alive cells
  private var aliveCells: HashSet[Cell] = new HashSet[Cell]()

  // Constructor from an array containing all living cells
  def this(aliveCells: Array[Cell]) {
    this()
    for(cell <- aliveCells) {
      add(cell)
    }
  }

  // Returns true if a cell is alive in this state; false otherwise
  def isAlive(cell: Cell): Boolean = aliveCells.contains(cell)

  //Iterate to next state
  def next() {
    val newAliveCells: HashSet[Cell] = new HashSet[Cell]()
    for(cell <- aliveCells) {
      // number of neighbors alive
      var neighborsAlive:Int = 0
      // Iterate through all neighbors and count how many are alive
      for(neighbor <- cell.getNeighbors) {
        if(this.isAlive(neighbor)) {
          //This neighbor is alive
          neighborsAlive = neighborsAlive + 1
        } else {
          // This neighbor is not alive,
          // However if it has exactly three alive neighbors it will be alive in next iteration.
          // So we need to check how many alive neighbors it has
          // Check only if we yet do not know if it is alive in next state
          if(!newAliveCells.contains(neighbor)) {
            val thisNeighbors = neighbor.getNeighbors
            var thisNeighborsAlive = 0
            for(thisNeighbor <- thisNeighbors) {
              if(this.isAlive(thisNeighbor)) {
                thisNeighborsAlive = thisNeighborsAlive + 1
              }
            }
            // If exactly three neighbors of this neighbor are alive then this neighbor will be alive
            if(thisNeighborsAlive == 3)
              newAliveCells.add(neighbor)
          }
        }
      }
      //If two or three neighbors are alive, then this cell will survive
      if( (neighborsAlive > 1) && (neighborsAlive < 4) )
        newAliveCells.add(cell)
    }
    aliveCells = newAliveCells
  }

  // Add a new living cell to this state
  def add(cell: Cell) {
    aliveCells.add(cell)
  }

  // Return all the alive cells
  def getAliveCells: Array[Cell] = aliveCells.toArray
}
