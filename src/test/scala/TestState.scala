import org.junit.Test
import org.scalatest.junit.AssertionsForJUnit
import scala.util.Sorting

/**
 * Created by Alexandru Grigoroi on 06/02/14. 
 */


class TestState extends AssertionsForJUnit {

  def checkTestCase(input: Array[Cell], expectedOutput: Array[Cell]) {
    Sorting.quickSort(expectedOutput)
    val state = new State(input)
    state.next()
    val output = state.getAliveCells
    Sorting.quickSort(output)
    assert(output.deep == expectedOutput.deep)
  }

  @Test def verifyNext() {
    var input: Array[Cell] = Array()
    var expectedOutput: Array[Cell] = Array()
    /*  In         Out
        1, 2       2, 1
        2, 2       2, 2
        3, 2       2, 3
       -1,-1      -1,-1 */
    input = Array(new Cell(1, 2), new Cell(2, 2), new Cell(3, 2))
    expectedOutput = Array(new Cell(2, 1), new Cell(2, 2), new Cell(2, 3))
    checkTestCase(input, expectedOutput)

    /*  In                     Out
        1, 2                   2, 1
        2, 2                   2, 2
        3, 2                   2, 3
        1000000001,1000000002 1000000002,1000000001
        1000000002,1000000002 1000000002,1000000002
        1000000003,1000000002 1000000002,1000000003
        -1,-1                  -1,-1 */
    input = Array(new Cell(1, 2), new Cell(2, 2), new Cell(3, 2), new Cell(1000000001,1000000002),
                  new Cell(1000000002,1000000002), new Cell(1000000003,1000000002))
    expectedOutput = Array(new Cell(2, 1), new Cell(2, 2), new Cell(2, 3), new Cell(1000000002,1000000001),
                           new Cell(1000000002,1000000002), new Cell(1000000002,1000000003))
    checkTestCase(input, expectedOutput)
  }
}
