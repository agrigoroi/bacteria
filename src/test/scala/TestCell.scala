/**
 * Created by Alexandru Grigoroi on 06/02/14. 
 */

import org.junit.Test
import org.scalatest.junit.AssertionsForJUnit
import scala.util.Sorting

class TestCell extends AssertionsForJUnit {

  @Test def verifyCompare() {
    assert(new Cell(2, 4) == new Cell(2, 4))
    assert(new Cell(2, 4) <= new Cell(2, 4))
    assert(new Cell(2, 3) <  new Cell(2, 4))
    assert(new Cell(3, 4) >  new Cell(2, 4))
  }

  def checkNeighbors(cell: Cell, expectedResult: Array[Cell]) {
    val neighbors = cell.getNeighbors
    Sorting.quickSort(neighbors)
    Sorting.quickSort(expectedResult)
    assert(neighbors.deep == expectedResult.deep)
  }

  @Test def verifyGetNeighbors() {
    // Cell 0, 0 will have only 3 neighbors with positive coordinates
    checkNeighbors(new Cell(0, 0), Array[Cell](new Cell(0, 1), new Cell(1, 0), new Cell(1, 1)))

    // Cells with x=0 xor y=0 will have 5 neighbors
    assert(new Cell(0, 14).getNeighbors.length == 5)
    assert(new Cell(23, 0).getNeighbors.length == 5)

    // Cells with x>0 and y>0 will have 8 neighbors
    assert(new Cell(43, 144).getNeighbors.length == 8)
  }
}
