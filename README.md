The code is located in `src/main/scala`. `Main.scala` contains a main method and can be ran from command-line.
It accepts two optional CLI arguments: the name of the input file and the name of the output.
If these arguments are not provided the program will use stdin and stdout
(Please *note* that sbt does not work well with programs that use stdin and stdout).

There are also some basic unit tests in src/test/scala, which check the functionality of some parts of the code.